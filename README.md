# TASK-MANAGER

## DEVELOPER

**NAME**: Anatoly Artamonov

**E-MAIL**: artamonov.av@hotmail.com

**E-MAIL**: aartamonov@t1-consulting.ru

## SOFTWARE

**JAVA**: JDK 1.8

**OS**:

* Windows 10 
* Ubuntu

## HARDWARE

**CPU**: i5

**RAM**: 16 Gb

**SSD**: 256 Gb

## APPLICATION RUN

```shell
java -jar tm-1.0.0.war
```

## APPLICATION BUILD

```shell
mvn clean install
```
