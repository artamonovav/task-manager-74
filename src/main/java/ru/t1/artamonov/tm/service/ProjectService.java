package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.repository.ProjectRepository;
import ru.t1.artamonov.tm.util.UserUtil;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Transactional
    public void add(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(UserUtil.getUserId());
        projectRepository.save(model);
    }

    @Override
    @Transactional
    public void addByUserId(@Nullable final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        projectRepository.save(model);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void clearByUserId(@Nullable final String userId) {
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void delete(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Override
    @Transactional
    public void deleteByUserId(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, model.getId());
    }


    @Override
    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByIdAndUserId(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Nullable
    public List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    @Nullable
    public Project findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    public Project findByUserIdAndId(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void update(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        if (findById(model.getId()) == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Override
    @Transactional
    public void updateByUserId(@Nullable String userId, @Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        if (findByUserIdAndId(userId, model.getId()) == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        projectRepository.save(model);
    }

}
