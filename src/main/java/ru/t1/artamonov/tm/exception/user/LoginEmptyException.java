package ru.t1.artamonov.tm.exception.user;

import ru.t1.artamonov.tm.exception.AbstractException;

public final class LoginEmptyException extends AbstractException {

    public LoginEmptyException() {
        super("Error! Login is not entered...");
    }

}
