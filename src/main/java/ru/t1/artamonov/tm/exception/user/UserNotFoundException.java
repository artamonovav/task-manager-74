package ru.t1.artamonov.tm.exception.user;

public final class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Error! UserDTO is not found...");
    }

}
