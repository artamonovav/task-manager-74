package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    @Transactional
    void add(@Nullable Project model);

    @Transactional
    void addByUserId(@Nullable String userId, @Nullable Project model);

    @Transactional
    void clear();

    @Transactional
    void clearByUserId(@Nullable String userId);

    @Transactional
    void delete(@Nullable Project model);

    @Transactional
    void deleteByUserId(@Nullable String userId, @Nullable Project model);

    @Transactional
    void deleteById(@Nullable String id);


    @Transactional
    void deleteByIdAndUserId(@Nullable String userId, @Nullable String id);

    @Nullable List<Project> findAll();

    @Nullable List<Project> findAllByUserId(@Nullable String userId);

    @Nullable Project findById(@Nullable String id);

    @Nullable Project findByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Transactional
    void update(@Nullable Project model);

    @Transactional
    void updateByUserId(@Nullable String userId, @Nullable Project model);

}
