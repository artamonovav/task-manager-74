package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    @Transactional
    void add(@Nullable Task model);

    @Transactional
    void addByUserId(@Nullable String userId, @Nullable Task model);

    @Transactional
    void clear();

    @Transactional
    void clearByUserId(@Nullable String userId);

    @Nullable List<Task> findAll();

    @Nullable List<Task> findAllByUserId(@Nullable String userId);

    @Nullable Task findById(@Nullable String id);

    @Nullable Task findByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Transactional
    void delete(@Nullable Task model);

    @Transactional
    void deleteByUserId(@Nullable String userId, @Nullable Task model);

    @Transactional
    void deleteById(@Nullable String id);

    @Transactional
    void deleteByIdAndUserId(@Nullable String userId, @Nullable String id);

    @Transactional
    void update(@Nullable Task model);

    @Transactional
    void updateByUserId(@Nullable String userId, @Nullable Task model);

}
