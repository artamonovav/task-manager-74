package ru.t1.artamonov.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.CustomUser;
import ru.t1.artamonov.tm.model.Project;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        projectService.addByUserId(user.getUserId(), new Project("New Project " + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.deleteByIdAndUserId(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute Project project, BindingResult result
    ) {
        projectService.updateByUserId(user.getUserId(), project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final Project project = projectService.findByUserIdAndId(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @Nullable
    public Status[] getStatuses() {
        return Status.values();
    }

}
