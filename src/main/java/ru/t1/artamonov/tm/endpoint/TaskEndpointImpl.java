package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.artamonov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.artamonov.tm.api.service.ITaskService;
import ru.t1.artamonov.tm.model.Task;
import ru.t1.artamonov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.artamonov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.deleteByUserId(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        taskService.deleteByIdAndUserId(UserUtil.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/create")
    public Task create(
            @WebParam(name = "id", partName = "id")
            @RequestBody final Task task
    ) {
        taskService.addByUserId(UserUtil.getUserId(), task);
        return task;
    }

    @NotNull
    @Override
    @WebMethod
    @PutMapping("/update")
    public Task update(
            @WebParam(name = "id", partName = "id")
            @RequestBody final Task task
    ) {
        taskService.updateByUserId(UserUtil.getUserId(), task);
        return task;
    }

}
