package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    void deleteAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    @Nullable
    List<Task> findAllByUserId(String userId);

    @Nullable
    Task findByUserIdAndId(String userId, String id);

}
