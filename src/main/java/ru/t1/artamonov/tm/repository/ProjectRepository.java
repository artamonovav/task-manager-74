package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    void deleteAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    @Nullable
    List<Project> findAllByUserId(String userId);

    @Nullable
    Project findByUserIdAndId(String userId, String id);


}
